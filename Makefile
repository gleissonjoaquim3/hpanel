CC=cc
CFLAGS= -O2 -I/usr/X11R6/include -DHAVE_XPM -DXFT -DCLOCK
LDFLAGS= -L/usr/X11R6/lib -lX11 -lXpm -lXft
PROGNAME=hpanel

$(PROGNAME): Makefile hpanel.c hpanel.h icon.xpm
	$(CC) $(CFLAGS) $(LDFLAGS) hpanel.c -o $(PROGNAME)
	@ls -l $(PROGNAME)
	strip $(PROGNAME)
	@ls -l $(PROGNAME)

install:
	install $(PROGNAME) /usr/local/bin
